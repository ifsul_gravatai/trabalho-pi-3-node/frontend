import { LocaleObject } from "yup/lib/locale";


export const yupErrorMessages: LocaleObject = {
    string: {
        email: 'Digite um email válido',
        min: 'O campo ${path} deve ter no mínimo ${min} caracteres',
    },
    mixed: {
        required: 'Campo obrigatório'
    }
}