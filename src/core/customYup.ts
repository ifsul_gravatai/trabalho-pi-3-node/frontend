import * as Yup from 'yup'
import { yupErrorMessages } from './yupErrosMessages'

Yup.setLocale(yupErrorMessages)

export { Yup }