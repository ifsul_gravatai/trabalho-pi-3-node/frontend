import { Box, AppBar, Toolbar, Typography, Button } from '@mui/material'
import { AuthConsumer } from '../../context/authContext'

export const Header = () => {
    const auth = AuthConsumer()

    return (
        <Box className="header">
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>IF Box</Typography>

                    <Button type="button" color="inherit" onClick={auth.logout}>Sair</Button>
                </Toolbar>
            </AppBar>
        </Box>
    )
}