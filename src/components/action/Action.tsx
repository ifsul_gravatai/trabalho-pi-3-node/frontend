import React from 'react'
import BackIcon from '../../assets/back.png'
import PlusIcon from '../../assets/plus.png'
import './style.scss'

interface IActionProps {
    type?: 'BACK' | 'CREATE'
    onClick?: () => void
}

export const Action: React.FC<IActionProps> = ({ type = 'BACK', onClick = () => {} }) => {
    return (
        <button type='button' className='action-wrapper' onClick={onClick}>
            <div className='icon-container'>
                {type === 'BACK' ? <img src={BackIcon} /> : <img src={PlusIcon} />}
            </div>
        </button>
    )
}