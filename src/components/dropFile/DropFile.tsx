import { FC, DragEvent, ChangeEvent, useState, createRef } from 'react'
import UploadFileIcon from '@mui/icons-material/UploadFile';

import './style.scss'

interface IDropFileProps {
    onSelectFile?: (file: FileList) => void,
    multiple?: boolean
}

export const DropFile: FC<IDropFileProps> = ({ onSelectFile, multiple }) => {
    const [isOver, setIsOver] = useState(false)
    const inputRef = createRef<HTMLInputElement>()

    const handleDragEnter = (event: DragEvent<HTMLDivElement>) => {
        setIsOver(true)
    }
    const handleDragOver = (event: DragEvent<HTMLDivElement>) => {
        event.preventDefault()
    }
    const handleDragLeave = (event: DragEvent<HTMLDivElement>) => {
        setIsOver(false)
    }
    const handleDrop = (event: DragEvent<HTMLDivElement>) => {
        event.preventDefault()
        setIsOver(false)

        handleFile(event.dataTransfer.files)
    }
    const handleClick = () => {
        inputRef.current?.click()
    }

    const handleInputFile = (event: ChangeEvent<HTMLInputElement>) => {
        handleFile(event.target.files)
    }

    const handleFile = (files: FileList | null) => {
        if (!files) {
            return
        }

        if (!onSelectFile) {
            return
        }

        onSelectFile(files)

        if (inputRef.current) {
            inputRef.current.value = ''
        }
    }


    return (
        <div className={`drop-file-wrapper ${isOver ? 'drag-over' : ''}`} onClick={handleClick} onDragEnter={handleDragEnter} onDragOver={handleDragOver} onDragLeave={handleDragLeave} onDrop={handleDrop}>
            <input ref={inputRef} type="file" onChange={handleInputFile} hidden multiple={multiple}/>

            <div className='content'>
                <UploadFileIcon sx={{ fontSize: '80px' }} color='action'/>
                Clique ou arrate para realizar o upload
            </div>
        </div>
    )
}