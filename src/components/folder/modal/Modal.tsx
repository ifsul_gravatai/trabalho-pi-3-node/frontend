import { Modal, Box, Typography, TextField, Button } from '@mui/material'
import { Formik } from 'formik';
import { Yup } from "../../../core/customYup";
import { IFolderItem } from '../../../services/folder/interfaces/folderGetDetails';

interface ICustomModalProps {
    modalStatus: boolean
    closeModal: () => void
    onClose: (obj: { name: string }) => void
    title: string
    buttonName: string
    item?: IFolderItem
}

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid grey',
    boxShadow: 24,
    p: 4
};

const customModalSchema = Yup.object().shape({
    name: Yup.string().test('path', 'Diretório não pode conter "/"', (value) => !/[/]/.test(value as string)).required()
})

export const CustomModal: React.FC<ICustomModalProps> = ({ modalStatus, closeModal, onClose, title, buttonName, item }) => {
    return (
        <Modal open={modalStatus} onClose={closeModal} aria-labelledby='custom-modal-title'>
            <Box sx={style}>
                <Typography id='custom-modal-title' variant='h6' component='h2'>{title}</Typography>

                <br/>

                <Formik initialValues={{ name: item?.name || '' }} validationSchema={customModalSchema} onSubmit={onClose}>
                    {({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
                        <form onSubmit={handleSubmit}>
                            <label style={{ marginBottom: '32px', display: 'block' }}>
                                <TextField fullWidth type='name' name='name' placeholder='Digite o nome do diretório' label='Nome' value={values.name} onChange={handleChange} onBlur={handleBlur} size='small' error={!!errors.name} helperText={errors.name}/>
                            </label>

                            <Button fullWidth type='submit' disabled={isSubmitting} variant='contained'>{buttonName}</Button>
                        </form>
                    )}
                </Formik>
            </Box>
        </Modal>
    )
}