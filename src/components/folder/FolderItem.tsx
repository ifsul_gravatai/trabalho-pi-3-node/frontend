import React from 'react'
import MoreVertIcon from '@mui/icons-material/MoreVert';
import FolderIcon from '../../assets/folder-icon.png'
import { format } from 'date-fns'
import { IconButton, Menu, MenuItem } from '@mui/material'
import { IFolderItem } from '../../services/folder/interfaces/folderGetDetails'
import { CustomModal } from './modal/Modal';
import './style.scss'
import { Link } from 'react-router-dom';

interface IFolderItemProps {
    item: IFolderItem
    onExclude?: (id: number) => void
    onRename?: (id: number, newName: string) => void
}

export const FolderItem: React.FC<IFolderItemProps> = ({ item, onExclude, onRename }) => {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const [modalStatus, setModalStatus] = React.useState(false);

    const open = Boolean(anchorEl);

    const openModal = () => setModalStatus(true);
    const closeModal = () => setModalStatus(false);

    const handleClick = (event: React.MouseEvent<HTMLElement>) => setAnchorEl(event.currentTarget);
    const closeMenu = () => setAnchorEl(null);

    const handleRename = ({ name }: { name: string }) => {
        if (name !== item.name) {
            onRename && onRename(item.id, name)
        }

        closeModal()
    }

    return (
        <div className='folder-container'>
            <CustomModal modalStatus={modalStatus} closeModal={closeModal} buttonName='Salvar' title='Renomear' onClose={handleRename} item={item}/>
            <div className='options-btn'>
                <IconButton onClick={handleClick}>
                    <MoreVertIcon></MoreVertIcon>
                </IconButton>

                <Menu anchorEl={anchorEl} open={open} onClose={closeMenu} onClick={closeMenu}>
                    <MenuItem onClick={() => onExclude && onExclude(item.id)}>Excluir</MenuItem>
                    <MenuItem onClick={openModal}>Renomear</MenuItem>
                </Menu>
            </div>
            <Link to={`?path=${item.path}`} className='folder-wrapper' >
                <div className='folder-item-wrapper'>
                    <img src={FolderIcon} />

                    <span className='name'>{item.name}</span>
                    <span className='modified'>Modified {format(new Date(item.updatedAt), 'dd/MM/yyyy')}</span>
                </div>
            </Link>
        </div>
    )
}