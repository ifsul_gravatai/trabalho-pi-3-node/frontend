
import { FC, useState, useEffect, useRef } from 'react'
import { ISocketConnection, IUseSocket, SocketEvents, useSocket } from '../../hooks/useSocket'
import { fileService } from '../../services/file/fileService'
import { IFileItem } from '../../services/folder/interfaces/folderGetDetails'
import './style.scss'

export interface IFileUpload {
    file: File,
    folderId: number
}

interface IFileUploadProps {
    item: IFileUpload,
    onFinish?: (folderId: number, file: IFileItem) => void
}

const formatBytes = (bytes: number, decimals = 2): string => {
    if (bytes === 0) {
        return '0 Bytes'
    }

    const k = 1024
    const dm = decimals < 0 ? 0 : decimals
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB']
    const i = Math.floor(Math.log(bytes) / Math.log(k))

    return (`${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`)
}

const calculatePercentage = (size: number, completed = 0): number => {
    const value = (completed * 100) / size

    return parseInt(value.toFixed(0))
}

export const FileUpload: FC<IFileUploadProps> = ({ item, onFinish }) => {
    const [socketId, setSocketId] = useState('')
    const [totalUploaded, setTotalUploaded] = useState(0)
    const socketRef = useRef<ISocketConnection>()
    const ioSocket = useSocket()

    const startUpload = async () => {
        if (!socketRef.current) {
            return
        }

        socketRef.current.on(SocketEvents.FILE_UPLOAD, (value: number) => {
            setTotalUploaded(value)
        })

        const formData = new FormData()

        formData.append('file', item.file)

        const { data } = await fileService.uploadFile(item.folderId, formData, socketRef.current.id)

        onFinish && onFinish(item.folderId, data.file)
    }

    const openConnection = async () => {
        socketRef.current = await ioSocket.connect()

        setSocketId(socketRef.current.id)
    }

    useEffect(() => {
        openConnection()

        return socketRef.current?.disconect()
    }, [])

    useEffect(() => {
        startUpload()
    }, [socketId])

    return(
        <div className='file-upload-wrapper'>
            <div className='file-upload-header'>
                <span>{item.file.name} | SocketID-{socketId}</span>
            </div>

            <div className='file-upload-info'>
                <div className='file-upload-progress-bar-wrapper'>
                    <div className='file-upload-progress-bar' style={{ width: `${calculatePercentage(item.file.size, totalUploaded)}%` }}></div>
                </div>

                <div className='profile-upload-progress-info'>
                    <span>{formatBytes(totalUploaded)} / {formatBytes(item.file.size)} ({calculatePercentage(item.file.size, totalUploaded)}%)</span>
                </div>
            </div>

        </div>
    )
}