import React from 'react'
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { IconButton, Menu, MenuItem } from '@mui/material'
import { IFileItem } from '../../services/folder/interfaces/folderGetDetails'
import { FileItemIcon } from './FileItemIcon';
import './style.scss'

interface IFileItemProps {
    item: IFileItem
    onExclude?: (id: number) => void
    onDownload?: (id: number) => void
}

export const FileItem: React.FC<IFileItemProps> = ({ item, onExclude, onDownload }) => {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    const open = Boolean(anchorEl);

    const handleClick = (event: React.MouseEvent<HTMLElement>) => setAnchorEl(event.currentTarget);
    const closeMenu = () => setAnchorEl(null);

    return (
        <div className='file-container' title={item.name}>
            <div className='options-btn'>
                <IconButton onClick={handleClick}>
                    <MoreVertIcon></MoreVertIcon>
                </IconButton>

                <Menu anchorEl={anchorEl} open={open} onClose={closeMenu} onClick={closeMenu}>
                    <MenuItem onClick={() => onExclude && onExclude(item.id)}>Excluir</MenuItem>
                    <MenuItem onClick={() => onDownload && onDownload(item.id)}>Baixar</MenuItem>
                </Menu>
            </div>
            <div className='file-item-wrapper'>
                <FileItemIcon type={item.type} ></FileItemIcon>

                <span className='name'>{item.name}</span>
            </div>
        </div>
    )
}