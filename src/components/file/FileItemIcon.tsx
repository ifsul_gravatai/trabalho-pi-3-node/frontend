import { FC } from 'react'
import { FileType } from '../../services/folder/enums/fileType'

interface IFileItemIconProps {
    type: FileType
}

export const FileItemIcon: FC<IFileItemIconProps> = ({ type }) => {
    return (
        <img src={`src/assets/files/${type}.png`} />
    )
}