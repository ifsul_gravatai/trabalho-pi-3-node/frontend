import { TextField, Button, Backdrop, CircularProgress } from '@mui/material';
import { AxiosError } from 'axios';
import { Formik } from 'formik';
import { useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { AuthService } from '../../services/auth/authService';
import { ILoginError } from '../../services/auth/interfaces/loginRequest';
import { ISigninBody } from '../../services/auth/interfaces/signinRequest';
import { signinFormSchema } from './signinFormSchema';
import './style.scss'

export function Signin() {
    const [isLoading, setIsLoading] = useState(false)
    const navigate = useNavigate()

    const submit = async (form: ISigninBody) => {
        setIsLoading(true)

        try {
            await AuthService.signin(form)

            toast.success('Cadastro efetuado com sucesso', {
                theme: 'colored',
                autoClose: 1000
            })

            navigate("/login")
        } catch (error) {
            let message = 'Houve um erro durante o cadastro'

            const isAxiosError = error instanceof AxiosError<ILoginError>

            if (isAxiosError && error.response) {
                const { response } = error as AxiosError<ILoginError>

                message = response?.data.message as string
            }

            toast.error(message, {
                theme: 'colored',
                autoClose: 1000
            })
        }

        setIsLoading(false)
    }

    return (
        <div className='full-size login-background'>
            <Backdrop open={isLoading} >
                <CircularProgress color="inherit" />
            </Backdrop>
            <div className='login-container'>
                <div className='login-container-header'>
                    <h1>Cadastro</h1>
                </div>

                <Formik initialValues={{ name: '', email: '', password: '' }} validationSchema={signinFormSchema} onSubmit={submit}>
                {({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
                    <form onSubmit={handleSubmit}>
                        <label>
                            <TextField type='text' name='name' placeholder='Digite seu nome' label='Nome' value={values.name} onChange={handleChange} onBlur={handleBlur} size='small' error={!!errors.name} helperText={errors.name}/>
                        </label>

                        <label>
                            <TextField type='email' name='email' placeholder='Digite seu email' label='Email' value={values.email} onChange={handleChange} onBlur={handleBlur} size='small' error={!!errors.email} helperText={errors.email}/>
                        </label>

                        <label>
                            <TextField type="password" name="password" placeholder='Digite sua senha' label="Senha" value={values.password} onChange={handleChange} onBlur={handleBlur} size='small' error={!!errors.password} helperText={errors.password} />
                        </label>

                        <Button type='submit' disabled={isSubmitting} variant='contained'>Cadastrar</Button>

                        <br />

                        <Link to='/login' style={{ textDecoration: 'none' }}>
                            <Button type='button' variant='outlined' fullWidth >Já tenho conta</Button>
                        </Link>
                    </form>
                )}
                </Formik>
            </div>
        </div>
    )
}