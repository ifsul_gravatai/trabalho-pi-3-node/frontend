import { Yup } from "../../core/customYup";

export const signinFormSchema = Yup.object().shape({
    name: Yup.string().required(),
    email: Yup.string().email().required(),
    password: Yup.string().min(6).required().label('senha')
})