import { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import { toast } from 'react-toastify'
import { Breadcrumbs, Link, Backdrop, CircularProgress } from '@mui/material'
import { FolderItem } from "../../components/folder/FolderItem"
import { useQuery } from "../../hooks/useQuery"
import { folderService } from "../../services/folder/folderService"
import { IFileItem, IFolderGetDetails } from "../../services/folder/interfaces/folderGetDetails"
import { Header } from "../../components/header/Header"
import { Action } from "../../components/action/Action"
import { CustomModal } from "../../components/folder/modal/Modal"
import { DropFile } from "../../components/dropFile/DropFile"
import { FileItem } from "../../components/file/FileItem"
import { FileUpload, IFileUpload } from "../../components/fileUpload/FileUpload"
import { fileService } from "../../services/file/fileService"
import './style.scss'

export const Home = () => {
    const query = useQuery()
    const navigate = useNavigate()

    const [data, setData] = useState<IFolderGetDetails>()
    const [filesToUpload, setFilesToUpload] = useState<IFileUpload[]>([])
    const [modalStatus, setModalStatus] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    const openModal = () => setModalStatus(true);
    const closeModal = () => setModalStatus(false);

    const requestDetails = async () => {
        const path = query.get('path') || ''

        setIsLoading(true)

        try {
            const { data } = await folderService.getDetails({ path })

            setData(data)
        } catch {
            toast.error('Erro ao procurar a pasta')
        }

        setIsLoading(false)
    }

    const buildLinks = () => {
        const path = query.get('path') || ''
        const splitedPath = path.split('/')

        return splitedPath.map(p => <Link sx={{ textDecoration: 'none', color: 'grey' }} href={`?path=${p}`} key={p}>{p || 'Root'}</Link>)
    }

    const handleBack = () => {
        const path = query.get('path') || ''

        const newPath = path.slice(0, path.lastIndexOf('/'))

        navigate(`?path=${newPath}`)
    }

    const handleCreate = async (form: { name: string }) => {
        if (!data) return

        closeModal()
        setIsLoading(true)

        try {
            await folderService.create(data.id, { folderName: form.name })
            await requestDetails()

            toast.success('Pasta criada')
        } catch {
            toast.error('Erro ao criar pasta')
        }
        setIsLoading(false)
    }

    const handleDeleteFolder = async (id: number) => {
        setIsLoading(true)

        try {
            await folderService.delete(id)

            if (!data) {
                return
            }

            const folders = data.folders.filter(folder => folder.id !== id)

            setData({
                ...data,
                folders
            })
            toast.success('Pasta removida')
        } catch (error) {
            toast.success('Erro ao remover pasta')
        }

        setIsLoading(false)
    }

    const handleDeleteFile = async (id: number) => {
        setIsLoading(true)

        try {
            await fileService.delete(id)

            if (!data) {
                return
            }

            const files = data?.files.filter(file => file.id !== id)

            setData({
                ...data,
                files
            })

            toast.success('Arquivo removido')
        } catch (error) {
            toast.success('Erro ao remover arquivo')
        }

        setIsLoading(false)

    }

    const handleDownload = (id: number) => {
        fileService.download(id).then((response) => {
            const blob = response.data

            const filename = extractFileNameFromResponse(response.headers)

            const href = URL.createObjectURL(blob)
            const link = document.createElement('a');

            link.href = href;
            link.setAttribute('download', filename);

            document.body.appendChild(link);

            link.click();

            document.body.removeChild(link);
            URL.revokeObjectURL(href);

        })
    }

    const extractFileNameFromResponse = (headers: any) => {
        const contentDisposition = headers.get('content-disposition');
        return contentDisposition.replace(/attachment; filename=|;/g, '').replace(/"/g, '')
      }

    const handleRename = async (id: number, name: string) => {
        setIsLoading(true)
        try {
            await folderService.rename(id, name)

            if (!data) {
                return toast.success('Pasta renomeada')
            }

            const newFolder = data?.folders.map(folder => {
                if (folder.id === id) {
                    folder.name = name
                }

                return folder
            })

            setData({
                id: data.id,
                folders: newFolder,
                files: data.files
            })

            toast.success('Pasta renomeada')
        } catch {
            toast.error('Erro ao renomear pasta')
        }
        setIsLoading(false)
    }

    const handleFile = async (files: FileList) => {
        if (!data) return

        const toFilesArray = Array.from(files)

        const filtered = toFilesArray.filter(f => {
            const { name } = f
            const { id } = data

            const alreadyHasFile = data.files.find(file => file.name === name)
            const alreadyHasFileToUpload = filesToUpload.find(file => file.folderId === id && file.file.name === name)

            if (alreadyHasFile) {
                toast.warning('Arquivo já existente')
            }

            if (alreadyHasFileToUpload) {
                toast.warning('Arquivo já selecionado')
            }

            return !alreadyHasFile
        })

        const mappedFiles = filtered.map((file): IFileUpload => ({
            file: file,
            folderId: data.id
        }))

        const filesList = [
            ...filesToUpload,
            ...mappedFiles
        ]

        setFilesToUpload(filesList)
    }

    const handleFinishFileUpload = (folderId: number, file: IFileItem) => {
        const newFilesToUpload = filesToUpload.filter(f => !(f.file.name === file.name && f.folderId === folderId))

        data?.files.push(file)
        setFilesToUpload(newFilesToUpload)
    }

    useEffect(() => {
        requestDetails()
    }, [query])

    return (
        <div className="full-size home-background">
            <Header />
            <Breadcrumbs sx={{ padding: '8px' }}>{buildLinks()}</Breadcrumbs>
            <Backdrop open={isLoading} >
                <CircularProgress color="inherit" />
            </Backdrop>
            <CustomModal modalStatus={modalStatus} closeModal={closeModal} buttonName='Salvar' title='Criar' onClose={handleCreate}/>
            <section style={{ padding: '16px' }}>
                <div className="items-wrapper">
                    <Action onClick={handleBack} />
                    {data?.folders.map(folder => <FolderItem key={folder.id} item={folder} onExclude={handleDeleteFolder} onRename={handleRename} />)}
                    {data?.files.map(file => <FileItem key={file.id} item={file} onExclude={handleDeleteFile} onDownload={handleDownload}></FileItem>)}
                    <Action onClick={openModal} type='CREATE' />
                </div>
                {filesToUpload.map(file => <FileUpload key={`${file.file.name}-${file.folderId}`} item={file} onFinish={handleFinishFileUpload} ></FileUpload>)}
                <DropFile onSelectFile={handleFile} />
            </section>
        </div>
    )
}