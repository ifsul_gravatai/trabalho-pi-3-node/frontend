import { Yup } from "../../core/customYup";

export const loginFormSchema = Yup.object().shape({
    email: Yup.string().email().required(),
    password: Yup.string().min(6).required().label('senha')
})