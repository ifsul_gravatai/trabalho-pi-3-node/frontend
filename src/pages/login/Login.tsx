import { TextField, Button, Backdrop, CircularProgress } from '@mui/material';
import { Formik } from 'formik';
import { useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { AuthConsumer } from '../../context/authContext';
import { ILoginBody } from '../../services/auth/interfaces/loginRequest';
import { loginFormSchema } from './loginFormSchema';
import './style.scss'

export function Login() {
    const [isLoading, setIsLoading] = useState(false)
    const auth = AuthConsumer()
    const navigate = useNavigate()

    const submit = async (form: ILoginBody) => {
        setIsLoading(true)

        const { success, message } = await auth.login(form)

        setIsLoading(false)

        if (success) {
            navigate("/")
        }

        toast.error(message, {
            theme: 'colored',
            autoClose: 1000
        })
    }

    return (
        <div className='full-size login-background'>
            <Backdrop open={isLoading} >
                <CircularProgress color="inherit" />
            </Backdrop>
            <div className='login-container'>
                <div className='login-container-header'>
                    <h1>Login</h1>
                </div>

                <Formik initialValues={{ email: '', password: '' }} validationSchema={loginFormSchema} onSubmit={submit}>
                {({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
                    <form onSubmit={handleSubmit}>
                        <label>
                            <TextField type='email' name='email' placeholder='Digite seu email' label='Email' value={values.email} onChange={handleChange} onBlur={handleBlur} size='small' error={!!(touched && errors.email)} helperText={errors.email}/>
                        </label>

                        <label>
                            <TextField type="password" name="password" placeholder='Digite sua senha' label="Senha" value={values.password} onChange={handleChange} onBlur={handleBlur} size='small' error={!!errors.password} helperText={errors.password} />
                        </label>

                        <Button type='submit' disabled={isSubmitting} variant='contained'>Login</Button>
                        <br />
                        <Link to='/signin' style={{ textDecoration: 'none' }}>
                            <Button type='button' variant='outlined' fullWidth >Cadastrar-se</Button>
                        </Link>
                    </form>
                )}
                </Formik>
            </div>
        </div>
    )
}