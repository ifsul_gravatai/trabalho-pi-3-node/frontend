export enum FileType {
    IMG = 'IMG',
    PDF = 'PDF',
    DOC = 'DOC',
    CSV = 'CSV',
    HTML = 'HTML',
    ISO = 'HTML',
    VIDEO = 'VIDEO',
    UNKNOWN = 'UNKNOWN'
}