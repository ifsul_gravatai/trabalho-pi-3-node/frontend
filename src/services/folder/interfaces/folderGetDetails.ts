import { FileType } from "../enums/fileType"

export interface IFolderItem {
    id: number
    name: string
    path: string
    updatedAt: Date
}

export interface IFileItem {
    id: number,
    name: string
    type: FileType
}

export interface IFolderGetDetails {
    id: number
    folders: IFolderItem[]
    files: IFileItem[]
}

export interface UploadFile {
    file: IFileItem
}