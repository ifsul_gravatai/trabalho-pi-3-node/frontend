import { AxiosResponse } from "axios";
import { privateInstace } from "../axios";
import { ICreateFolder } from "./interfaces/createFolder";
import { IFolderGetDetails } from "./interfaces/folderGetDetails";

class FolderService {
    public async getDetails(params: Record<string, string>): Promise<AxiosResponse<IFolderGetDetails>> {
        return privateInstace.get<IFolderGetDetails>('/folder', { params })
    }

    public async create(id: number, body: ICreateFolder): Promise<AxiosResponse> {
        return privateInstace.post(`/folder/${id}`, body)
    }

    public async rename(id: number, name: string): Promise<AxiosResponse<IFolderGetDetails>> {
        return privateInstace.patch(`/folder/${id}/name`, { name })
    }

    public async delete(id: number): Promise<AxiosResponse> {
        return privateInstace.delete(`/folder/${id}`)
    }
}

export const folderService = new FolderService();