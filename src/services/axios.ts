import axios, { AxiosError } from 'axios'

export const publicInstance = axios.create({ baseURL: 'http://localhost:3001' })

export const privateInstace = axios.create({ baseURL: 'http://localhost:3001' })

publicInstance.interceptors.response.use((config) => {
    return config
}, (error) => {
    const isAxiosError = error instanceof AxiosError

    if (isAxiosError && error.response?.status === 401) {
        window.location.href = '/login';
    }
    return Promise.reject(error)
})

privateInstace.interceptors.request.use((config) => {
    const token = sessionStorage.getItem('token')

    config.headers.Authorization = token

    return config
}, (error) => {
    return Promise.reject(error)
})