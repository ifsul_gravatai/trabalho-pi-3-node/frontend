import { AxiosResponse } from "axios";
import { privateInstace } from "../axios";
import { UploadFile } from "../folder/interfaces/folderGetDetails";

class FileService {
    public async uploadFile(folderId: number, body: FormData, socketId: string): Promise<AxiosResponse<UploadFile>> {
        return privateInstace.post<UploadFile>(`/folder/${folderId}/file`, body, {
            params: {
                socketId
            },
            headers: {
                "Content-Type": "multipart/form-data",
            }
        })
    }

    public async delete(fileId: number): Promise<AxiosResponse> {
        return privateInstace.delete(`/file/${fileId}`)
    }

    public async download(fileId: number): Promise<AxiosResponse> {
        return privateInstace.get(`/file/${fileId}/download`, { responseType: 'blob' })
    }
}

export const fileService = new FileService();