import { publicInstance } from "../axios"
import { ILoginBody, ILoginPayload } from "./interfaces/loginRequest"
import { AxiosResponse } from 'axios'
import { ISigninBody } from "./interfaces/signinRequest"

class Service {
    public async login(body: ILoginBody): Promise<AxiosResponse<ILoginPayload>> {
        return publicInstance.post<ILoginPayload>('/auth/login', body)
    }

    public async signin(body: ISigninBody): Promise<AxiosResponse> {
        return publicInstance.post('/auth/signin', body)
    }
}

export const AuthService = new Service();