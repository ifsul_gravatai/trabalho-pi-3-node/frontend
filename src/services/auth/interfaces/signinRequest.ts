export interface ISigninBody {
    name: string
    email: string
    password: string
}
