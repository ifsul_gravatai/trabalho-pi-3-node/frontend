export interface ILoginPayload {
    token: string
}

export interface ILoginBody {
    email: string
    password: string
}

export interface ILoginError {
    message: string | string[] | Record<string, string>[]
}
