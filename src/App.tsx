import { AuthProvider } from "./context/authContext";
import { AppRoutes } from "./routes/Routes";
import './style.scss'

export function App() {
  return <>
    <div id="pages-wrapper">
      <AuthProvider>
        <AppRoutes />
      </AuthProvider>
    </div>
  </>
}