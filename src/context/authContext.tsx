import * as React from 'react'
import { IUseAuth, useAuth } from "../hooks/authHook"

interface IAuthProviderProps {
    children: JSX.Element
}

const AuthContext = React.createContext<IUseAuth | null>(null)

export const AuthProvider = ({ children }: IAuthProviderProps) => {
    const auth = useAuth()

    return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>
}

export const AuthConsumer = () => {
    const consumer = React.useContext(AuthContext)

    if (!consumer) {
        throw new Error('AuthConsumer is null')
    }

    return consumer
}