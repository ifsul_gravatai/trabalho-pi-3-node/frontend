import { Fragment } from 'react'
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom'
import { Home } from '../pages/home/Home'
import { Login } from '../pages/login/Login'
import { Signin } from '../pages/singin/Signin'
import { PrivateRoute } from './PrivateRoute'


export const AppRoutes = () => {
    return (
        <Router>
            <Fragment>
                <Routes>
                    <Route path='/' element={<PrivateRoute component={<Home />} />} />
                    <Route path='/login' element={<Login />} />
                    <Route path='/signin' element={<Signin />} />
                </Routes>
            </Fragment>
        </Router>
    )
}