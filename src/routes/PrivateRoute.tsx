import React from "react";
import { Navigate } from "react-router-dom";
import { AuthConsumer } from "../context/authContext";

interface IPrivateRouteProps {
    component: JSX.Element
}

export const PrivateRoute: React.FC<IPrivateRouteProps> = ({ component }): JSX.Element => {
    const { authenticated } = AuthConsumer()

    if (authenticated) {
        return component
    }

    return <Navigate to='/login' />
}