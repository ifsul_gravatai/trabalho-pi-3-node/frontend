import io, { Socket } from 'socket.io-client'

export enum SocketEvents {
    CONNECT = 'connect',
    DISCONECT = 'disconect',
    FILE_UPLOAD = 'file-upload'
}

type EventHandler = (event: SocketEvents, callback: (data: any) => void) => void

export interface ISocketConnection {
    socket: Socket
    id: string,
    on: EventHandler
    off: EventHandler
    disconect: () => void
}

export interface IUseSocket {
    connect: () => Promise<ISocketConnection>
}

export const useSocket = (): IUseSocket => {
    const connect = () => {
        const socket = io('http://localhost:3001')

        return new Promise<ISocketConnection>((resolve) => {
            socket.on(SocketEvents.CONNECT, () => {


                const on: EventHandler = (event, callback) => {
                    socket.on(event, callback)
                }

                const off: EventHandler = (event, callback) => {
                    socket.on(event, callback)
                }

                const disconect = () => {
                    socket.disconnect()
                }

                resolve({
                    socket,
                    id: socket.id,
                    on,
                    off,
                    disconect
                })
            })
        })
    }


    return {
        connect
    }
}