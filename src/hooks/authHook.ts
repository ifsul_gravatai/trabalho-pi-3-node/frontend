import { useState } from "react"
import { AxiosError } from 'axios'
import jwt from 'jwt-decode'
import { AuthService } from "../services/auth/authService"
import { ILoginBody, ILoginError } from "../services/auth/interfaces/loginRequest"

export interface IUseAuth {
    authenticated: boolean,
    login: (body: ILoginBody) => Promise<LoginResponse>,
    logout: () => void
}

interface LoginResponse {
    success: boolean,
    message?: any
}

interface DecodedToken {
    id: number,
    exp: number,
    iat: number
}

const isAuthenticated = () => {
    const token = sessionStorage.getItem('token') as string

    if (!token) return false

    const decoded = jwt<DecodedToken>(token)

    return !(decoded.exp * 1000 < Date.now())
}

export const useAuth = (): IUseAuth => {
    const [authenticated, setAuthenticated] = useState(isAuthenticated())

    const login = async (body: ILoginBody): Promise<LoginResponse> => {
        try {
            const response = await AuthService.login(body)

            sessionStorage.setItem('token', response.data.token)
            setAuthenticated(true)

            return { success: true };
        } catch (error) {

            if (error instanceof AxiosError) {
                const message = (error as AxiosError<ILoginError>).response?.data.message

                return { success: false, message }
            }

            return { success: false }
        }
    }

    const logout = () => {
        sessionStorage.removeItem('token')
        setAuthenticated(false)
    }

    return {
        authenticated,
        login,
        logout
    }
}